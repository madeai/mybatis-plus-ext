package com.tangzc.mpe.autotable.strategy.dm.data;

import com.tangzc.mpe.autotable.strategy.dm.data.enums.DMDefaultTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

/**
 * @author don
 */
@Data
@NoArgsConstructor
public class DMTypeAndLength {
    private Integer length;
    private Integer decimalLength;
    private String type;

    public DMTypeAndLength(Integer length, Integer decimalLength, String type) {
        if (length != null && length >= 0) {
            this.length = length;
        }
        if (decimalLength != null && decimalLength >= 0) {
            this.decimalLength = decimalLength;
        }
        this.type = type;
    }

    private static final Set<String> CHAR_STRING_TYPE = new HashSet<String>() {{
        add(DMDefaultTypeEnum.CHAR.typeName());
        add(DMDefaultTypeEnum.CHARACTER.typeName());
        add(DMDefaultTypeEnum.VARCHAR.typeName());
        add(DMDefaultTypeEnum.VARCHAR2.typeName());
        add(DMDefaultTypeEnum.LONGVARCHAR.typeName());
    }};

    private static final Set<String> DATE_TIME_TYPE = new HashSet<String>() {{
        add(DMDefaultTypeEnum.DATE.typeName());
        add(DMDefaultTypeEnum.DATETIME.typeName());
        add(DMDefaultTypeEnum.TIMESTAMP.typeName());
        add(DMDefaultTypeEnum.TIME_WITH_TIME_ZONE.typeName());
    }};

    private static final Set<String> INTEGER_TYPE = new HashSet<String>() {{
        add(DMDefaultTypeEnum.INT.typeName());
        add(DMDefaultTypeEnum.INTEGER.typeName());
        add(DMDefaultTypeEnum.BIGINT.typeName());
        add(DMDefaultTypeEnum.SMALLINT.typeName());
        add(DMDefaultTypeEnum.TINYINT.typeName());
    }};

    private static final Set<String> FLOAT_TYPE = new HashSet<String>() {{
        add(DMDefaultTypeEnum.FLOAT.typeName());
        add(DMDefaultTypeEnum.DOUBLE.typeName());
        add(DMDefaultTypeEnum.DOUBLE_PRECISION.typeName());
        add(DMDefaultTypeEnum.REAL.typeName());
        add(DMDefaultTypeEnum.NUMBER.typeName());
        add(DMDefaultTypeEnum.NUMERIC.typeName());
        add(DMDefaultTypeEnum.DECIMAL.typeName());
        add(DMDefaultTypeEnum.DEC.typeName());
    }};

    public String typeName() {
        return this.type.toLowerCase();
    }

    public String getFullType() {
        // 例：double(4,2)
        String typeAndLength = this.typeName();
        // 类型具备长度属性 且 自定义长度不为空

        if (this.length != null) {
            typeAndLength += "(" + this.length;
            if (this.decimalLength != null) {
                typeAndLength += "," + this.decimalLength;
            }
            typeAndLength += ")";
        } else if (this.decimalLength != null) {
            typeAndLength += "(" + this.decimalLength + ")";
        }
        return typeAndLength;
    }

    public boolean isCharString() {
        return CHAR_STRING_TYPE.contains(this.typeName());
    }

    public boolean isDateTime() {
        return DATE_TIME_TYPE.contains(this.typeName());
    }

    public boolean needStringCompatibility() {
        return isCharString() || isDateTime();
    }

    public boolean isBoolean() {
        return DMDefaultTypeEnum.BIT.typeName().equalsIgnoreCase(this.typeName());
    }

    public boolean isNumber() {
        return (INTEGER_TYPE.contains(this.typeName()) || FLOAT_TYPE.contains(this.typeName()));
    }

    public boolean isFloatNumber() {
        return FLOAT_TYPE.contains(this.typeName());
    }

    public boolean isNoLengthNumber() {
        return INTEGER_TYPE.contains(this.typeName());
    }
}
