package com.tangzc.mpe.autotable.strategy.dm;

import com.tangzc.mpe.autotable.annotation.enums.DefaultValueEnum;
import com.tangzc.mpe.autotable.annotation.enums.IndexSortTypeEnum;
import com.tangzc.mpe.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mpe.autotable.constants.DatabaseDialect;
import com.tangzc.mpe.autotable.properties.AutoTableProperties;
import com.tangzc.mpe.autotable.strategy.IStrategy;
import com.tangzc.mpe.autotable.strategy.dm.builder.CreateTableSqlBuilder;
import com.tangzc.mpe.autotable.strategy.dm.builder.ModifyTableSqlBuilder;
import com.tangzc.mpe.autotable.strategy.dm.builder.TableMetadataBuilder;
import com.tangzc.mpe.autotable.strategy.dm.data.DMColumnMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMCompareTableInfo;
import com.tangzc.mpe.autotable.strategy.dm.data.DMIndexMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMTableMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.dbdata.DMDbColumn;
import com.tangzc.mpe.autotable.strategy.dm.data.dbdata.DMDbIndex;
import com.tangzc.mpe.autotable.strategy.dm.data.dbdata.DMDbPrimary;
import com.tangzc.mpe.autotable.strategy.dm.mapper.DMTablesMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author yangdingming
 * @date 2024/2/19 14:33
 **/
@Slf4j
public class DMStrategy implements IStrategy<DMTableMetadata, DMCompareTableInfo> {
    @Resource
    private DMTablesMapper dmTablesMapper;
    @Resource
    private AutoTableProperties autoTableProperties;

    @Override
    public DatabaseDialect dbDialect() {
        return DatabaseDialect.DM;
    }

    @Override
    public void dropTable(String tableName) {
        dmTablesMapper.dropTableByName(tableName);
    }

    @Override
    public boolean checkTableExist(String tableName) {
        return dmTablesMapper.findTableCountByTableName(tableName) > 0;
    }

    @Override
    public DMTableMetadata analyseClass(Class<?> beanClass) {
        DMTableMetadata dmTableMetadata = TableMetadataBuilder.build(beanClass);
        if (dmTableMetadata.getColumnMetadataList().isEmpty()) {
            log.warn("扫描发现{}没有建表字段请检查！", beanClass.getName());
            return null;
        }
        return dmTableMetadata;
    }

    @Override
    public void createTable(DMTableMetadata tableMetadata) {
        String buildSql = CreateTableSqlBuilder.buildSql(tableMetadata);
        log.info("开始创建表：{}", tableMetadata.getTableName());
        log.info("执行SQL：{}", buildSql);
//        达梦JDBC在执行多条SQL语句时会在dm.jdbc.driver.DmdbPreparedStatement#prepareSql方法中报错, 所以这里按;分割一条一条执行
        Arrays.stream(buildSql.split(";")).filter(StringUtils::hasText).forEachOrdered(dmTablesMapper::executeSql);
        log.info("结束创建表：{}", tableMetadata.getTableName());
    }

    @Override
    public DMCompareTableInfo compareTable(DMTableMetadata tableMetadata) {
        String tableName = tableMetadata.getTableName();

        DMCompareTableInfo dmCompareTableInfo = new DMCompareTableInfo(tableName);

        // 比较表信息
        compareTableInfo(tableMetadata, tableName, dmCompareTableInfo);

        // 比较字段信息
        compareColumnInfo(tableMetadata, tableName, dmCompareTableInfo);

        // 比较索引信息
        compareIndexInfo(tableMetadata, tableName, dmCompareTableInfo);

        return dmCompareTableInfo;
    }

    private void compareIndexInfo(DMTableMetadata tableMetadata, String tableName, DMCompareTableInfo dmCompareTableInfo) {
        List<DMDbIndex> dmDbIndices = dmTablesMapper.selectTableIndexesDetail(tableName);
        Map<String, List<DMDbIndex>> dmDbIndexMap = dmDbIndices.stream()
                // 仅仅处理自定义的索引
                .filter(idx -> idx.getIndexName().startsWith(autoTableProperties.getIndexPrefix()))
                // 按ColumnPosition升序
                .sorted(Comparator.comparing(DMDbIndex::getColumnPosition))
                .collect(Collectors.groupingBy(DMDbIndex::getIndexName, Collectors.toList()));

        List<DMIndexMetadata> indexMetadataList = tableMetadata.getIndexMetadataList();
        for (DMIndexMetadata dmIndexMetadata : indexMetadataList) {
            String indexName = dmIndexMetadata.getName();
            List<DMDbIndex> dbIndex = dmDbIndexMap.remove(indexName);
            // 新增索引
            if (dbIndex == null) {
                // 标记索引信息
                dmCompareTableInfo.addNewIndex(dmIndexMetadata);
                continue;
            }

            // 比较索引UNIQUE: phone DESC, car ASC
            boolean isUniqueIndex = dmIndexMetadata.getType() == IndexTypeEnum.UNIQUE;
            String indexColumnParams = (isUniqueIndex ? "UNIQUE: " : "") + dmIndexMetadata.getColumns().stream().map(col -> col.getColumn() + (col.getSort() == IndexSortTypeEnum.DESC ? " DESC" : " ASC")).collect(Collectors.joining(", "));
            String dbIndexColumnParams = ("UNIQUE".equals(dbIndex.get(0).getUniqueness()) ? "UNIQUE: " : "") + dbIndex.stream().map(col -> col.getColumnName() + " " + col.getDescend()).collect(Collectors.joining(", "));
            if (!Objects.equals(indexColumnParams, dbIndexColumnParams)) {
                dmCompareTableInfo.addModifyIndex(dmIndexMetadata);
            }
        }

        // 需要删除的索引
        Set<String> needRemoveIndexes = dmDbIndexMap.keySet();
        if (!needRemoveIndexes.isEmpty()) {
            dmCompareTableInfo.addDropIndexes(needRemoveIndexes);
        }
    }

    private void compareColumnInfo(DMTableMetadata tableMetadata, String tableName, DMCompareTableInfo dmCompareTableInfo) {
        // 数据库字段元信息
        List<DMDbColumn> dmDbColumns = dmTablesMapper.selectTableFieldDetail(tableName);
        Map<String, DMDbColumn> dmFieldDetailMap = dmDbColumns.stream().collect(Collectors.toMap(DMDbColumn::getColumnName, Function.identity()));
        // 当前字段信息
        List<DMColumnMetadata> columnMetadataList = tableMetadata.getColumnMetadataList();

        for (DMColumnMetadata dmColumnMetadata : columnMetadataList) {
            String columnName = dmColumnMetadata.getName();
            DMDbColumn dmDbColumn = dmFieldDetailMap.remove(columnName);
            // 新增字段
            if (dmDbColumn == null) {
                // 标记注释
                dmCompareTableInfo.addColumnComment(dmColumnMetadata.getName(), dmColumnMetadata.getComment());
                // 标记字段信息
                dmCompareTableInfo.addNewColumn(dmColumnMetadata);
                continue;
            }
            // @Column注解中的comment默认为""并且数据库中的comment为null时不更改数据库中的comment
            boolean isEmptyString = dmDbColumn.getComments() == null && "".equals(dmColumnMetadata.getComment());
            if (!isEmptyString && !Objects.equals(dmDbColumn.getComments(), dmColumnMetadata.getComment())) {
                dmCompareTableInfo.addColumnComment(columnName, dmColumnMetadata.getComment());
            }
            /* 修改的字段 */

            // 主键忽略判断，单独处理
            if (!dmColumnMetadata.isPrimary()) {
                // 字段类型不同
                boolean isTypeDiff = isTypeDiff(dmColumnMetadata, dmDbColumn);
                // 非null不同
                boolean isNotnullDiff = dmColumnMetadata.isNotNull() == Objects.equals(dmDbColumn.getNullable(), "Y");
                // 默认值不同
                boolean isDefaultDiff = isDefaultDiff(dmColumnMetadata, dmDbColumn.getDataDefault());
                if (isTypeDiff || isNotnullDiff || isDefaultDiff) {
//                    log.info("diff->bean:[{} {} {}]  db:[{} {} {}]", dmColumnMetadata.getType().getFullType().toLowerCase(), dmColumnMetadata.isNotNull(), dmColumnMetadata.getDefaultValue(), dmDbColumn.getDataTypeFormat().toLowerCase(), Objects.equals(dmDbColumn.getNullable(), "N"), dmDbColumn.getDataDefault());
                    dmCompareTableInfo.addModifyColumn(dmColumnMetadata);
                }
            }
        }
        // 需要删除的字段
        Set<String> needRemoveColumns = dmFieldDetailMap.keySet();
        if (!needRemoveColumns.isEmpty()) {
            dmCompareTableInfo.addDropColumns(needRemoveColumns);
        }

        /* 处理主键 */
        // 获取所有主键
        List<DMColumnMetadata> primaryColumnList = columnMetadataList.stream().filter(DMColumnMetadata::isPrimary).collect(Collectors.toList());
        // 查询数据库主键信息
        DMDbPrimary dmDbPrimary = dmTablesMapper.selectPrimaryKeyName(tableName);

        boolean removePrimary = primaryColumnList.isEmpty() && dmDbPrimary != null;
        String newPrimaryColumns = primaryColumnList.stream().map(DMColumnMetadata::getName).collect(Collectors.joining(","));
        boolean primaryChange = dmDbPrimary != null && !Objects.equals(dmDbPrimary.getColumns(), newPrimaryColumns);
        if (removePrimary || primaryChange) {
            // 标记待删除的主键
            dmCompareTableInfo.setDropPrimaryKeyName(dmDbPrimary.getPrimaryName());
        }
        boolean newPrimary = !primaryColumnList.isEmpty() && dmDbPrimary == null;
        if (newPrimary || primaryChange) {
            // 标记新创建的主键
            dmCompareTableInfo.addNewPrimary(primaryColumnList);
        }
    }

    private void compareTableInfo(DMTableMetadata tableMetadata, String tableName, DMCompareTableInfo dmCompareTableInfo) {
        String tableComment = dmTablesMapper.selectTableComment(tableName);
        if (!Objects.equals(tableComment, tableMetadata.getComment())) {
            dmCompareTableInfo.setComment(tableMetadata.getComment());
        }
    }

    //    todo: DMDefaultTypeEnum里的默认字段长度需要根据达梦数据库建表的默认字段长度再对一下
    private static boolean isTypeDiff(DMColumnMetadata dmColumnMetadata, DMDbColumn dmDbColumn) {
        String dataTypeFormat = dmDbColumn.getDataTypeFormat().toLowerCase();
        String fullType = dmColumnMetadata.getType().getFullType().toLowerCase();
        return !Objects.equals(fullType, dataTypeFormat);
    }

    private static boolean isDefaultDiff(DMColumnMetadata dmColumnMetadata, String columnDefault) {
        DefaultValueEnum defaultValueType = dmColumnMetadata.getDefaultValueType();
        if (DefaultValueEnum.isValid(defaultValueType)) {
            if (defaultValueType == DefaultValueEnum.EMPTY_STRING) {
                return !"''".equals(columnDefault);
            }
            if (defaultValueType == DefaultValueEnum.NULL) {
                return columnDefault != null;
            }
        } else {
            String defaultValue = dmColumnMetadata.getDefaultValue();
            return !Objects.equals(defaultValue, columnDefault);
        }
        return false;
    }

    @Override
    public void modifyTable(DMCompareTableInfo compareTableInfo) {
        String buildSql = ModifyTableSqlBuilder.buildSql(compareTableInfo);
        log.info("开始修改表：{}", compareTableInfo.getName());
        log.info("执行SQL：{}", buildSql);
//        达梦JDBC在执行多条SQL语句时会在dm.jdbc.driver.DmdbPreparedStatement#prepareSql方法中报错, 所以这里按;分割一条一条执行
        Arrays.stream(buildSql.split(";")).filter(StringUtils::hasText).forEachOrdered(dmTablesMapper::executeSql);
        log.info("结束修改表：{}", compareTableInfo.getName());
    }
}
