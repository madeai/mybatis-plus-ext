package com.tangzc.mpe.autotable.strategy.dm.converter.impl;

import com.tangzc.mpe.autotable.strategy.dm.converter.JavaToDMConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 如果没有{@link JavaToDMConverter}的实现类，则默认加载该类
 * @author don
 */
@Configuration
@ConditionalOnMissingBean(JavaToDMConverter.class)
public class DefaultJavaToDMConverterDefine {

    @Bean
    public DefaultJavaToDMConverter defaultJavaToDMConverter() {
        return new DefaultJavaToDMConverter();
    }
}
