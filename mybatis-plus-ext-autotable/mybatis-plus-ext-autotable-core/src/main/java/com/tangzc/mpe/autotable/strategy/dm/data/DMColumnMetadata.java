package com.tangzc.mpe.autotable.strategy.dm.data;

import com.tangzc.mpe.autotable.annotation.ColumnDefault;
import com.tangzc.mpe.autotable.annotation.ColumnType;
import com.tangzc.mpe.autotable.annotation.enums.DefaultValueEnum;
import com.tangzc.mpe.autotable.strategy.dm.converter.JavaToDMConverter;
import com.tangzc.mpe.autotable.utils.StringHelper;
import com.tangzc.mpe.autotable.utils.TableBeanUtils;
import com.tangzc.mpe.magic.TableColumnNameUtil;
import com.tangzc.mpe.magic.util.SpringContextUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;

/**
 * @author yangdingming
 * @date 2024/2/20 9:57
 **/
@Data
@Slf4j
public class DMColumnMetadata {
    /**
     * 字段名: 不可变，变了意味着新字段
     */
    private String name;

    /**
     * 字段的备注
     */
    private String comment;

    /**
     * 字段类型
     */
    private DMTypeAndLength type;

    /**
     * 字段是否非空
     */
    private boolean notNull;

    /**
     * 主键是否自增
     */
    private boolean autoIncrement;

    /**
     * <p>字段默认值类型</p>
     * <p>如果该值有值的情况下，将忽略 {@link #defaultValue} 的值</p>
     */
    private DefaultValueEnum defaultValueType;

    /**
     * <p>字段默认值</p>
     * <p>如果 {@link #defaultValueType} 有值的情况下，将忽略本字段的指定</p>
     */
    private String defaultValue;

    /**
     * 字段是否是主键
     */
    private boolean primary;

    public static DMColumnMetadata create(Class<?> clazz, Field field) {
        DMColumnMetadata dmColumnMetadata = new DMColumnMetadata();
        dmColumnMetadata.setName(TableColumnNameUtil.getRealColumnName(field));
        dmColumnMetadata.setType(getTypeAndLength(field, clazz));
        dmColumnMetadata.setNotNull(TableBeanUtils.isNotNull(field));
        dmColumnMetadata.setPrimary(TableBeanUtils.isPrimary(field));
        dmColumnMetadata.setAutoIncrement(TableBeanUtils.isAutoIncrement(field));
        ColumnDefault columnDefault = TableBeanUtils.getDefaultValue(field);
        if (columnDefault != null) {
            dmColumnMetadata.setDefaultValueType(columnDefault.type());
            String defaultValue = columnDefault.value();
            if (StringUtils.hasText(defaultValue)) {
                DMTypeAndLength type = dmColumnMetadata.getType();
                // 补偿逻辑：类型为Boolean的时候(实际数据库为bit数字类型)，兼容 true、false
                if (type.isBoolean() && !"1".equals(defaultValue) && !"0".equals(defaultValue)) {
                    defaultValue = Boolean.parseBoolean(defaultValue) ? "1" : "0";
                }
                // 补偿逻辑：需要兼容字符串的类型，前后自动添加'
                if (type.isCharString() && !defaultValue.isEmpty() && !defaultValue.startsWith("'") && !defaultValue.endsWith("'")) {
                    defaultValue = "'" + defaultValue + "'";
                }
                // 补偿逻辑：时间类型，非函数的值，前后自动添加'
                if (type.isDateTime() && defaultValue.matches("(\\d+.?)+") && !defaultValue.startsWith("'") && !defaultValue.endsWith("'")) {
                    defaultValue = "'" + defaultValue + "'";
                }
                dmColumnMetadata.setDefaultValue(defaultValue);
            }
        }
        dmColumnMetadata.setComment(TableBeanUtils.getComment(field));

        return dmColumnMetadata;
    }

    /**
     * 生成字段相关的SQL片段
     */
    public String toColumnSql() {
        // 例子：name            VARCHAR(50)
        // 例子：id              VARCHAR(50) not null
        return StringHelper.newInstance("\"{columnName}\" {typeAndLength} {null} {default} {autoIncrement}")
                .replace("{columnName}", this.getName())
                .replace("{typeAndLength}", this.getType().getFullType())
                .replace("{null}", this.isNotNull() ? "NOT NULL" : "NULL")
                .replace("{default}", ($) -> {
                    // 指定NULL
                    DefaultValueEnum defaultValueType = this.getDefaultValueType();
                    if (defaultValueType == DefaultValueEnum.NULL) {
                        return "DEFAULT NULL";
                    }
                    // 指定空字符串
                    if (defaultValueType == DefaultValueEnum.EMPTY_STRING) {
                        return "DEFAULT ''";
                    }
                    // 自定义
                    String defaultValue = this.getDefaultValue();
                    if (DefaultValueEnum.isCustom(defaultValueType) && StringUtils.hasText(defaultValue)) {
                        return "DEFAULT " + defaultValue;
                    }
                    return "";
                })
                .replace("{autoIncrement}", this.isAutoIncrement() ? "AUTO_INCREMENT" : "")
                .toString();
    }

    private static DMTypeAndLength getTypeAndLength(Field field, Class<?> clazz) {


        // 类型为空根据字段类型去默认匹配类型
        Class<?> fieldType = TableBeanUtils.getFieldType(clazz, field);
        // 获取外部注入的自定义
        JavaToDMConverter javaToDMConverter = SpringContextUtil.getBeanOfType(JavaToDMConverter.class);
        DMTypeAndLength typeAndLength = javaToDMConverter.convert(fieldType);

        ColumnType column = TableBeanUtils.getColumnType(field);
        if (column != null) {
            // 如果重新设置了类型，则长度也需要重新设置
            if (StringUtils.hasText(column.value()) && !column.value().equalsIgnoreCase(typeAndLength.getType())) {
                typeAndLength.setType(column.value());
                typeAndLength.setLength(null);
                typeAndLength.setDecimalLength(null);
            }
            if (column.length() >= 0) {
                typeAndLength.setLength(column.length());
            }
            if (column.decimalLength() >= 0) {
                typeAndLength.setDecimalLength(column.decimalLength());
            }
        }

        return typeAndLength;
    }
}
