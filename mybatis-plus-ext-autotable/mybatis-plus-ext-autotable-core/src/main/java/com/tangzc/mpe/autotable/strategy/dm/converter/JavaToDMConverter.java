package com.tangzc.mpe.autotable.strategy.dm.converter;

import com.tangzc.mpe.autotable.strategy.dm.data.DMTypeAndLength;

/**
 * 自定义java转达梦的类型转换器
 * @author don
 */
@FunctionalInterface
public interface JavaToDMConverter {

    /**
     * java转达梦类型
     */
    DMTypeAndLength convert(Class<?> fieldClass);
}
