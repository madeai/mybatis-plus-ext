package com.tangzc.mpe.autotable.strategy.dm.data.dbdata;

import com.tangzc.mpe.autotable.strategy.dm.data.DMTypeConstant;
import lombok.Data;

import java.util.Date;
import java.util.Objects;

/**
 * @author yangdingming
 * @date 2024/2/21 11:13
 **/
@Data
public class DMDbColumn {
    /**
     * 所属表名
     */
    private String tableName;
    /**
     * 字段名称
     */
    private String columnName;
    /**
     * 数据类型
     */
    private String dataType;
    private String dataTypeMod;
    private String dataTypeOwner;
    /**
     * 数据长度
     */
    private Long dataLength;
    private Long dataPrecision;
    /**
     * 数据精度, 对应decimalLength
     */
    private Long dataScale;
    /**
     * 是否可以为null Y or N
     */
    private String nullable;
    private Long columnId;
    private Long defaultLength;
    /**
     * 列默认值
     */
    private String dataDefault;
    private Long numDistinct;
    private String lowValue;
    private String highValue;
    private Long density;
    private Long numNulls;
    private Long numBuckets;
    private Date lastAnalyzed;
    private Long sampleSize;
    /**
     * 字符集名称 CHAR_CS
     */
    private String characterSetName;
    private Long charColDeclLength;
    private String globalStats;
    private String userStats;
    private Long avgColLen;
    private Long charLength;
    private String charUsed;
    private String v80FmtImage;
    private String dataUpgraded;
    private String histogram;
    /**
     * 字段注释
     */
    private String comments;

    /**
     * @return dataType
     */
    public String getDataTypeFormat() {
        switch (this.dataType) {
            case "BYTE":
            case "CHAR":
            case "CHARACTER":
            case "VARCHAR":
            case "VARCHAR2":
            case "LONGVARCHAR":
                return this.dataType + "(" + this.dataLength + ")";
            case "FLOAT":
            case "DOUBLE":
            case "DOUBLE_PRECISION":
            case "REAL":
            case "NUMBER":
            case "NUMERIC":
            case "DECIMAL":
            case "DEC":
            case "TIMESTAMP":
                return this.dataType + (this.dataScale == null || 0L == this.dataScale ? "" : "(" + this.dataScale + ")");
            default:
                return this.dataType;
        }
    }
}
