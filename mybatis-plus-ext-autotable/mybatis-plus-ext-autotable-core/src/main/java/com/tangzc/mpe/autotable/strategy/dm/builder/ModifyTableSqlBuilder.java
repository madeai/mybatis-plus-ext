package com.tangzc.mpe.autotable.strategy.dm.builder;

import com.tangzc.mpe.autotable.strategy.dm.data.DMColumnMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMCompareTableInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author yangdingming
 * @date 2024/2/21 9:56
 **/
@Slf4j
public class ModifyTableSqlBuilder {
    /**
     * 构建修改表的SQL
     *
     * @param dmCompareTableInfo 参数
     * @return SQL语句
     */
    public static String buildSql(DMCompareTableInfo dmCompareTableInfo) {
        String tableName = dmCompareTableInfo.getName();

        String tableComment = dmCompareTableInfo.getComment();
        Map<String, String> columnComment = dmCompareTableInfo.getColumnComment();

        /* 修改字段 */
        List<String> alterTableSqlList = new ArrayList<>();
        // 删除主键
        String primaryKeyName = dmCompareTableInfo.getDropPrimaryKeyName();
        if (StringUtils.hasText(primaryKeyName)) {
            alterTableSqlList.add("DROP CONSTRAINT " + primaryKeyName);
        }
        // 删除列
        List<String> dropColumnList = dmCompareTableInfo.getDropColumnList();
        dropColumnList.stream()
                .map(columnName -> "DROP COLUMN " + columnName)
                .forEach(alterTableSqlList::add);
        // 新增列
        List<DMColumnMetadata> newColumnList = dmCompareTableInfo.getNewColumnMetadataList();
        newColumnList.stream()
                .map(column -> "ADD COLUMN " + column.toColumnSql())
                .forEach(alterTableSqlList::add);
        // 修改列
        List<DMColumnMetadata> modifyColumnList = dmCompareTableInfo.getModifyColumnMetadataList();
        modifyColumnList.stream()
                .map(column -> "MODIFY " + column.toColumnSql())
                .forEach(alterTableSqlList::add);

        // 添加主键
        List<DMColumnMetadata> newPrimaries = dmCompareTableInfo.getNewPrimaries();
        if (!newPrimaries.isEmpty()) {
            String primaryColumns = newPrimaries.stream().map(col -> "\"" + col.getName() + "\"").collect(Collectors.joining(", "));
            if (StringUtils.hasText(primaryKeyName)) {
                alterTableSqlList.add("ADD CONSTRAINT \"" + primaryKeyName + "\" PRIMARY KEY (" + primaryColumns + ")");
            } else {
                alterTableSqlList.add("ADD PRIMARY KEY (" + primaryColumns + ")");
            }
        }
        // 组合sql
        String alterTableSql = alterTableSqlList.isEmpty() ? "" : alterTableSqlList.stream().map(a -> "ALTER TABLE \"" + tableName + "\" " + a +";").collect(Collectors.joining("\n"));

        /* 为 表、字段 添加注释 */
        String addColumnCommentSql = CreateTableSqlBuilder.getAddColumnCommentSql(tableName, tableComment, columnComment);

        /* 修改 索引 */
        // 删除索引
        List<String> dropIndexList = dmCompareTableInfo.getDropIndexList();
        String dropIndexSql = dropIndexList.stream().map(indexName -> "DROP INDEX \"" + indexName + "\";").collect(Collectors.joining("\n"));
        // 添加索引
        String createIndexSql = CreateTableSqlBuilder.getCreateIndexSql(tableName, dmCompareTableInfo.getIndexMetadataList());


        return dropIndexSql + " " + alterTableSql + " " + addColumnCommentSql + " " + createIndexSql;
    }
}
