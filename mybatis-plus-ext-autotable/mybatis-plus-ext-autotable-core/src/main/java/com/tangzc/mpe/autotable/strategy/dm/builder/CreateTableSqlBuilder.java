package com.tangzc.mpe.autotable.strategy.dm.builder;

import com.tangzc.mpe.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mpe.autotable.strategy.dm.data.DMColumnMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMIndexMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMTableMetadata;
import com.tangzc.mpe.autotable.utils.StringHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author don
 */
@Slf4j
public class CreateTableSqlBuilder {

    /**
     * 构建创建新表的SQL
     *
     * @param dmTableMetadata 参数
     * @return sql
     */
    public static String buildSql(DMTableMetadata dmTableMetadata) {
        String tableName = dmTableMetadata.getTableName();
        List<DMIndexMetadata> indexMetadataList = dmTableMetadata.getIndexMetadataList();
        return getCreateTableSql(dmTableMetadata) + " "
                + addTableCommentSql(dmTableMetadata) + " "
                + getCreateIndexSql(tableName, indexMetadataList);
    }


    private static String addTableCommentSql(DMTableMetadata dmTableMetadata) {
        String tableName = dmTableMetadata.getTableName();
        String tableComment = dmTableMetadata.getComment();
        return "COMMENT ON TABLE \"{tableName}\" IS '{tableComment}';"
                .replace("{tableName}", tableName)
                .replace("{tableComment}", tableComment);
    }


    private static String getCreateTableSql(DMTableMetadata dmTableMetadata) {
        String name = dmTableMetadata.getTableName();
        List<DMColumnMetadata> dmColumnMetadataList = dmTableMetadata.getColumnMetadataList();

        // 记录所有修改项，（利用数组结构，便于添加,分割）
        List<String> addItems = new ArrayList<>();

        // 获取所有主键（至于表字段处理之前，为了主键修改notnull）
        List<String> primaries = new ArrayList<>();
        dmColumnMetadataList.forEach(columnData -> {
            // 判断是主键，自动设置为NOT NULL，并记录
            if (columnData.isPrimary()) {
                columnData.setNotNull(true);
                primaries.add(columnData.getName());
            }
        });

        // 表字段处理
        addItems.add(
                dmColumnMetadataList.stream()
                        // 拼接每个字段的sql片段
                        .map(DMColumnMetadata::toColumnSql)
                        .collect(Collectors.joining(","))
        );

        // 主键
        if (!primaries.isEmpty()) {
            String primaryKeySql = getPrimaryKeySql(primaries);
            addItems.add(primaryKeySql);
        }

        // 组合sql: 过滤空字符项，逗号拼接
        String addSql = addItems.stream()
                .filter(StringUtils::hasText)
                .collect(Collectors.joining(","));

        return "CREATE TABLE \"{tableName}\" ({addItems});"
                .replace("{tableName}", name)
                .replace("{addItems}", addSql);
    }

    public static String getCreateIndexSql(String tableName, List<DMIndexMetadata> indexMetadataList) {
        return indexMetadataList.stream()
                .map(dmIndexMetadata -> StringHelper.newInstance("CREATE {indexType} INDEX \"{indexName}\" ON \"{tableName}\" ({columns});")
                        .replace("{indexType}", dmIndexMetadata.getType() == IndexTypeEnum.UNIQUE ? "UNIQUE" : "")
                        .replace("{indexName}", dmIndexMetadata.getName())
                        .replace("{tableName}", tableName)
                        .replace("{columns}", (key) -> {
                            List<DMIndexMetadata.IndexColumnParam> columnParams = dmIndexMetadata.getColumns();
                            return columnParams.stream().map(column ->
                                    // 例："name" ASC
                                    "\"{column}\" {sortMode}"
                                            .replace("{column}", column.getColumn())
                                            .replace("{sortMode}", column.getSort() != null ? column.getSort().name() : "")
                            ).collect(Collectors.joining(","));
                        })
                        .toString()
                ).collect(Collectors.joining(" "));
    }

    public static String getPrimaryKeySql(List<String> primaries) {
        return "NOT CLUSTER PRIMARY KEY ({primaries})"
                .replace(
                        "{primaries}",
                        String.join(",", primaries)
                );
    }

    public static String getAddColumnCommentSql(String tableName, String tableComment, Map<String, String> columnCommentMap) {
        List<String> commentList = new ArrayList<>();

        // 表备注
        if (StringUtils.hasText(tableComment)) {
            String addTableComment = "COMMENT ON TABLE \"{tableName}\" IS '{comment}';"
                    .replace("{tableName}", tableName)
                    .replace("{comment}", tableComment);
            commentList.add(addTableComment);
        }

        // 字段备注
        columnCommentMap.entrySet().stream()
                .map(columnComment -> "COMMENT ON COLUMN \"{tableName}\".\"{name}\" IS '{comment}';"
                        .replace("{tableName}", tableName)
                        .replace("{name}", columnComment.getKey())
                        .replace("{comment}", columnComment.getValue()))
                .forEach(commentList::add);

        return String.join(" ", commentList);
    }
}
