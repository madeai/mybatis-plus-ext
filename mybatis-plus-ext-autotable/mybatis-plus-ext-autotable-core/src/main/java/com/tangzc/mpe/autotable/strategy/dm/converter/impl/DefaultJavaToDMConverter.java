package com.tangzc.mpe.autotable.strategy.dm.converter.impl;

import com.tangzc.mpe.autotable.strategy.dm.converter.JavaToDMConverter;
import com.tangzc.mpe.autotable.strategy.dm.data.DMTypeAndLength;
import com.tangzc.mpe.autotable.strategy.dm.data.enums.DMDefaultTypeEnum;
import com.tangzc.mpe.magic.util.EnumUtil;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 依据: https://blog.csdn.net/ahriall/article/details/114260398
 * @author yangdingming
 */
public class DefaultJavaToDMConverter implements JavaToDMConverter {

    public static final Map<Class<?>, DMDefaultTypeEnum> JAVA_TO_DM_TYPE_MAP = new HashMap<Class<?>, DMDefaultTypeEnum>() {{
        put(BigDecimal.class, DMDefaultTypeEnum.DECIMAL);
        put(String.class, DMDefaultTypeEnum.VARCHAR);
        put(Boolean.class, DMDefaultTypeEnum.BIT);
        put(boolean.class, DMDefaultTypeEnum.BIT);
        put(Integer.class, DMDefaultTypeEnum.INT);
        put(int.class, DMDefaultTypeEnum.INT);
        put(Long.class, DMDefaultTypeEnum.BIGINT);
        put(long.class, DMDefaultTypeEnum.BIGINT);
        put(Byte.class,DMDefaultTypeEnum.BYTE);
        put(byte.class,DMDefaultTypeEnum.BYTE);
        put(Short.class, DMDefaultTypeEnum.SMALLINT);
        put(short.class, DMDefaultTypeEnum.SMALLINT);
        put(Byte[].class,DMDefaultTypeEnum.BINARY);
        put(byte[].class,DMDefaultTypeEnum.BINARY);
        put(Double.class, DMDefaultTypeEnum.DOUBLE);
        put(double.class, DMDefaultTypeEnum.DOUBLE);
        put(Float.class, DMDefaultTypeEnum.REAL);
        put(float.class, DMDefaultTypeEnum.REAL);
        put(Date.class, DMDefaultTypeEnum.DATE);
        put(java.sql.Date.class, DMDefaultTypeEnum.DATE);
        put(java.sql.Timestamp.class, DMDefaultTypeEnum.TIMESTAMP);
        put(java.sql.Time.class, DMDefaultTypeEnum.TIMESTAMP);

        put(Character.class, DMDefaultTypeEnum.CHAR);
        put(char.class, DMDefaultTypeEnum.CHAR);
        put(BigInteger.class, DMDefaultTypeEnum.BIGINT);
        put(LocalDateTime.class, DMDefaultTypeEnum.TIMESTAMP);
        put(LocalDate.class, DMDefaultTypeEnum.DATE);
        put(LocalTime.class, DMDefaultTypeEnum.DATETIME);
    }};

    @Override
    public DMTypeAndLength convert(Class<?> fieldClass) {

        if (fieldClass.isEnum()) {
            // 枚举默认设置字符串类型
            fieldClass = EnumUtil.getEnumFieldSaveDbType(fieldClass);
        }
        DMDefaultTypeEnum sqlType = JAVA_TO_DM_TYPE_MAP.getOrDefault(fieldClass, DMDefaultTypeEnum.VARCHAR);

        if (sqlType == null) {
            throw new RuntimeException(fieldClass + "默认情况下，不支持转换到达梦类型，如有需要请自行实现" + JavaToDMConverter.class.getName());
        }

        return new DMTypeAndLength(sqlType.getLengthDefault(), sqlType.getDecimalLengthDefault(), sqlType.name().toLowerCase());
    }
}
