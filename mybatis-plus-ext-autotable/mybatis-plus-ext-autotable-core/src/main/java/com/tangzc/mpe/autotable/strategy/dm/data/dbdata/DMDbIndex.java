package com.tangzc.mpe.autotable.strategy.dm.data.dbdata;

import lombok.Data;

/**
 * 达梦数据库，索引信息
 */
@Data
public class DMDbIndex {
    /**
     * 索引名称
     */
    private String indexName;
    /**
     * 所属表名称
     */
    private String tableName;
    /**
     * 字段名称
     */
    private String columnName;
    /**
     * 字段位置
     */
    private Long columnPosition;
    private Long columnLength;
    private Long charLength;
    /**
     * ASC
     */
    private String descend;
    /**
     * 是否唯一约束: UNIQUE, NONUNIQUE
     */
    private String uniqueness;
}
