package com.tangzc.mpe.autotable.strategy.dm.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.tangzc.mpe.autotable.strategy.dm.data.dbdata.DMDbColumn;
import com.tangzc.mpe.autotable.strategy.dm.data.dbdata.DMDbIndex;
import com.tangzc.mpe.autotable.strategy.dm.data.dbdata.DMDbPrimary;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yangdingming
 * @date 2024/2/19 14:43
 **/
@InterceptorIgnore(tenantLine = "true", illegalSql = "true", blockAttack = "true")
public interface DMTablesMapper {
    /**
     * 达梦JDBC在执行多条SQL语句时会在dm.jdbc.driver.DmdbPreparedStatement#prepareSql方法中报错
     * @param sql 单条SQL
     */
    @Select("${sql}")
    void executeSql(String sql);

    @Select("SELECT COUNT(*) FROM USER_TABLES WHERE TABLE_NAME ='${tableName}'")
    int findTableCountByTableName(String tableName);

    @Delete("DROP TABLE IF EXISTS ${tableName}")
    void dropTableByName(String tableName);

    @Select("SELECT COMMENTS FROM ALL_TAB_COMMENTS WHERE TABLE_NAME='${tableName}' AND TABLE_TYPE='TABLE' AND OWNER=(SELECT SYS_CONTEXT('USERENV','CURRENT_SCHEMA'))")
    String selectTableComment(String tableName);

    @Select("SELECT a.*, b.COMMENTS\n " +
            "FROM USER_TAB_COLUMNS a\n " +
            "LEFT JOIN USER_COL_COMMENTS b ON a.TABLE_NAME = b.TABLE_NAME AND a.COLUMN_NAME = b.COLUMN_NAME\n " +
            "WHERE a.TABLE_NAME = '${tableName}'")
    List<DMDbColumn> selectTableFieldDetail(String tableName);

    @Select("SELECT LISTAGG(a.COLUMN_NAME, ',') WITHIN GROUP(ORDER BY a.COLUMN_POSITION) AS columns, b.CONSTRAINT_NAME AS primary_name\n " +
            "FROM USER_IND_COLUMNS a LEFT JOIN USER_CONSTRAINTS b\n " +
            "ON a.INDEX_NAME=b.INDEX_NAME AND a.TABLE_NAME=b.TABLE_NAME\n " +
            "where b.CONSTRAINT_TYPE='P' AND a.TABLE_NAME='${tableName}'")
    DMDbPrimary selectPrimaryKeyName(String tableName);

    @Select("SELECT a.*, b.UNIQUENESS\n" +
            "FROM USER_IND_COLUMNS a\n" +
            "LEFT JOIN USER_INDEXES b ON a.INDEX_NAME = b.INDEX_NAME AND a.TABLE_NAME = b.TABLE_NAME\n" +
            "WHERE a.TABLE_NAME = '${tableName}'\n" +
            "AND a.INDEX_NAME NOT IN(SELECT INDEX_NAME FROM USER_CONSTRAINTS WHERE CONSTRAINT_TYPE == 'P');")
    List<DMDbIndex> selectTableIndexesDetail(String tableName);
}
