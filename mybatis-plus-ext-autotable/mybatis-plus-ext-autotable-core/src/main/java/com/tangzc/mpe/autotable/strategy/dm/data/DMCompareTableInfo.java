package com.tangzc.mpe.autotable.strategy.dm.data;

import com.tangzc.mpe.autotable.strategy.CompareTableInfo;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author yangdingming
 * @date 2024/2/19 14:34
 **/
@Data
@RequiredArgsConstructor
public class DMCompareTableInfo implements CompareTableInfo {
    /**
     * 表名: 不可变，变了意味着新表
     */
    @NonNull
    private final String name;

    /**
     * 注释: 有值说明需要改
     */
    private String comment;

    /**
     * 新的主键
     */
    private List<DMColumnMetadata> newPrimaries = new ArrayList<>();
    /**
     * 不为空删除主键
     */
    private String dropPrimaryKeyName;

    /**
     * 注释: 需要添加/修改的字段注释《列名，注释内容》
     */
    private Map<String, String> columnComment = new HashMap<>();

    /**
     * 需要删除的列
     */
    private List<String> dropColumnList = new ArrayList<>();

    /**
     * 需要修改的列
     */
    private List<DMColumnMetadata> modifyColumnMetadataList = new ArrayList<>();

    /**
     * 需要新增的列
     */
    private List<DMColumnMetadata> newColumnMetadataList = new ArrayList<>();

    /**
     * 需要删除的索引
     */
    private List<String> dropIndexList = new ArrayList<>();

    /**
     * 新添加的索引
     */
    private List<DMIndexMetadata> indexMetadataList = new ArrayList<>();

    @Override
    public boolean needModify() {
        return StringUtils.hasText(comment) ||
                StringUtils.hasText(dropPrimaryKeyName) ||
                !newPrimaries.isEmpty() ||
                !columnComment.isEmpty() ||
                !dropColumnList.isEmpty() ||
                !modifyColumnMetadataList.isEmpty() ||
                !newColumnMetadataList.isEmpty() ||
                !dropIndexList.isEmpty() ||
                !indexMetadataList.isEmpty();
    }

    public void addColumnComment(String columnName, String newComment) {
        this.columnComment.put(columnName, newComment);
    }

    public void addNewColumn(DMColumnMetadata columnMetadata) {
        this.newColumnMetadataList.add(columnMetadata);
    }

    public void addModifyColumn(DMColumnMetadata columnMetadata) {
        this.modifyColumnMetadataList.add(columnMetadata);
    }

    public void addDropColumns(Set<String> dropColumnList) {
        this.dropColumnList.addAll(dropColumnList);
    }

    public void addNewIndex(DMIndexMetadata dmIndexMetadata) {
        this.indexMetadataList.add(dmIndexMetadata);
    }

    public void addModifyIndex(DMIndexMetadata dmIndexMetadata) {
        this.dropIndexList.add(dmIndexMetadata.getName());
        this.indexMetadataList.add(dmIndexMetadata);
    }

    public void addDropIndexes(Set<String> indexNameList) {
        this.dropIndexList.addAll(indexNameList);
    }

    public void addNewPrimary(List<DMColumnMetadata> dmColumnMetadata) {
        this.newPrimaries.addAll(dmColumnMetadata);
    }
}
