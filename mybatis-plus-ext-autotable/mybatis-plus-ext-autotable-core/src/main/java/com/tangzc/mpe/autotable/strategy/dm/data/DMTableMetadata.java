package com.tangzc.mpe.autotable.strategy.dm.data;

import com.tangzc.mpe.autotable.strategy.TableMetadata;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdingming
 * @date 2024/2/19 14:33
 **/
@Data
public class DMTableMetadata implements TableMetadata {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 注释
     */
    private String comment;
    /**
     * 所有列
     */
    private List<DMColumnMetadata> columnMetadataList = new ArrayList<>();
    /**
     * 索引
     */
    private List<DMIndexMetadata> indexMetadataList = new ArrayList<>();
}
