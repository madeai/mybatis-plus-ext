package com.tangzc.mpe.autotable.strategy.dm.data.dbdata;

import lombok.Data;

/**
 * @author yangdingming
 * @date 2024/2/21 17:19
 **/
@Data
public class DMDbPrimary {
    /**
     * 主键名称
     */
    private String primaryName;
    /**
     * 主键列的拼接,例子：name,phone
     */
    private String columns;
}
