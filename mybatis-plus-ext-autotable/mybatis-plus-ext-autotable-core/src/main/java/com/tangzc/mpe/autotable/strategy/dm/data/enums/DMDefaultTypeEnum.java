package com.tangzc.mpe.autotable.strategy.dm.data.enums;

import com.tangzc.mpe.autotable.strategy.dm.data.DMTypeConstant;
import lombok.Getter;

/**
 * 用于配置Mysql数据库中类型，并且该类型需要设置几个长度
 * 这里配置多少个类型决定了，创建表能使用多少类型
 * 例如：varchar(1)
 * decimal(5,2)
 * datetime
 *
 * @author don
 */
@Getter
public enum DMDefaultTypeEnum {

    /**
     * 整数
     */
    INT(DMTypeConstant.INT,null,null),
    INTEGER(DMTypeConstant.INTEGER,null,null),
    BIGINT(DMTypeConstant.BIGINT,null,null),
    SMALLINT(DMTypeConstant.SMALLINT,null,null),
    TINYINT(DMTypeConstant.TINYINT,null,null),
    BYTE(DMTypeConstant.BYTE,1,null),
    /**
     * 小数
     */
    FLOAT(DMTypeConstant.FLOAT,null,2),
    DOUBLE(DMTypeConstant.DOUBLE,null,2),
    DOUBLE_PRECISION(DMTypeConstant.DOUBLE_PRECISION,null,2),
    REAL(DMTypeConstant.REAL,null,2),
    NUMBER(DMTypeConstant.NUMBER,null,4),
    NUMERIC(DMTypeConstant.NUMERIC,null,4),
    DECIMAL(DMTypeConstant.DECIMAL,null,6),
    DEC(DMTypeConstant.DEC,null,4),
    /**
     * 字符串
     */
    CHAR(DMTypeConstant.CHAR,255,null),
    CHARACTER(DMTypeConstant.CHARACTER,255,null),
    VARCHAR(DMTypeConstant.VARCHAR,255,null),
    VARCHAR2(DMTypeConstant.VARCHAR2,255,null),
    LONGVARCHAR(DMTypeConstant.LONGVARCHAR,1024,null),
    /**
     * 日期
     */
    DATE(DMTypeConstant.DATE,null,null),
    DATETIME(DMTypeConstant.DATETIME,null,null),
    TIMESTAMP(DMTypeConstant.TIMESTAMP,null,6),
    TIME_WITH_TIME_ZONE(DMTypeConstant.TIME_WITH_TIME_ZONE,null,null),
    /**
     * 二进制
     */
    BIT(DMTypeConstant.BIT,null,null),
    BINARY(DMTypeConstant.BINARY,1,null),
    VARBINARY(DMTypeConstant.VARBINARY,1,null),
    BLOB(DMTypeConstant.BLOB,null,null),
    TEXT(DMTypeConstant.TEXT,null,null),
    CLOB(DMTypeConstant.CLOB,null,null);

    /**
     * 默认类型长度
     */
    private final Integer lengthDefault;
    /**
     * 默认小数点后长度
     */
    private final Integer decimalLengthDefault;
    /**
     * 类型名称
     */
    private final String typeName;

    DMDefaultTypeEnum(String typeName, Integer lengthDefault, Integer decimalLengthDefault) {
        this.typeName = typeName;
        this.lengthDefault = lengthDefault;
        this.decimalLengthDefault = decimalLengthDefault;
    }

    public String typeName() {
        return this.typeName;
    }
}
