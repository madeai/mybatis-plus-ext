package com.tangzc.mpe.autotable.strategy.dm.builder;

import com.tangzc.mpe.autotable.annotation.Table;
import com.tangzc.mpe.autotable.annotation.TableIndex;
import com.tangzc.mpe.autotable.properties.AutoTableProperties;
import com.tangzc.mpe.autotable.strategy.dm.data.DMColumnMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMIndexMetadata;
import com.tangzc.mpe.autotable.strategy.dm.data.DMTableMetadata;
import com.tangzc.mpe.autotable.utils.IndexRepeatChecker;
import com.tangzc.mpe.autotable.utils.TableBeanUtils;
import com.tangzc.mpe.magic.BeanClassUtil;
import com.tangzc.mpe.magic.TableColumnNameUtil;
import com.tangzc.mpe.magic.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author don
 */
@Slf4j
public class TableMetadataBuilder {

    private static AutoTableProperties autoTableProperties;

    public static DMTableMetadata build(Class<?> clazz) {

        String tableName = TableColumnNameUtil.getTableName(clazz);

        DMTableMetadata dmTableMetadata = new DMTableMetadata();
        dmTableMetadata.setTableName(tableName);

        Table tableAnno = AnnotatedElementUtils.findMergedAnnotation(clazz, Table.class);
        assert tableAnno != null;
        // 获取表注释
        dmTableMetadata.setComment(tableAnno.comment());
        

        List<Field> fields = BeanClassUtil.getAllDeclaredFieldsExcludeStatic(clazz);
        dmTableMetadata.setColumnMetadataList(getColumnList(clazz, fields));
        dmTableMetadata.setIndexMetadataList(getIndexList(clazz, fields));

        return dmTableMetadata;
    }

    public static List<DMColumnMetadata> getColumnList(Class<?> clazz, List<Field> fields) {
        return fields.stream()
                .filter(field -> TableBeanUtils.isIncludeField(field, clazz))
                .map(field -> DMColumnMetadata.create(clazz, field))
                .collect(Collectors.toList());
    }

    public static List<DMIndexMetadata> getIndexList(Class<?> clazz, List<Field> fields) {

        IndexRepeatChecker indexRepeatChecker = IndexRepeatChecker.of();

        // 类上的索引注解
        List<TableIndex> tableIndexes = TableBeanUtils.getTableIndexes(clazz);
        List<DMIndexMetadata> indexMetadataList = tableIndexes.stream()
                .map(tableIndex -> DMIndexMetadata.create(clazz, tableIndex, getAutoTableProperties().getIndexPrefix()))
                .filter(Objects::nonNull)
                .filter(indexMetadata -> indexRepeatChecker.filter(indexMetadata.getName()))
                .collect(Collectors.toList());

        // 字段上的索引注解
        List<DMIndexMetadata> onFieldIndexMetadata = fields.stream()
                .filter(field -> TableBeanUtils.isIncludeField(field, clazz))
                .map(field -> DMIndexMetadata.create(field, getAutoTableProperties().getIndexPrefix()))
                .filter(Objects::nonNull)
                .filter(indexMetadata -> indexRepeatChecker.filter(indexMetadata.getName()))
                .collect(Collectors.toList());
        indexMetadataList.addAll(onFieldIndexMetadata);
        return indexMetadataList;
    }

    public static AutoTableProperties getAutoTableProperties() {

        if(autoTableProperties == null) {
            autoTableProperties = SpringContextUtil.getBeanOfType(AutoTableProperties.class);
        }

        return autoTableProperties;
    }
}
