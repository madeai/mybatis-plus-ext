package com.tangzc.mpe.autotable.strategy.dm.data;

public interface DMTypeConstant {

    /**
     * 整数
     */
    String INT = "int";
    String INTEGER = "integer";
    String BIGINT = "bigint";
    String SMALLINT = "smallint";
    String TINYINT = "tinyint";
    String BYTE = "byte";
    /**
     * 小数
     */
    String FLOAT = "float";
    String DOUBLE = "double";
    String DOUBLE_PRECISION = "double precision";
    String REAL = "real";
    String NUMBER = "number";
    String NUMERIC = "numeric";
    String DECIMAL = "decimal";
    String DEC = "dec";
    /**
     * 字符串
     */
    String CHAR = "char";
    String CHARACTER = "character";
    String VARCHAR = "varchar";
    String VARCHAR2 = "varchar2";
    String LONGVARCHAR = "longvarchar";
    /**
     * 日期
     */
    String DATE = "date";
    String DATETIME = "datetime";
    String TIMESTAMP = "timestamp";
    String TIME_WITH_TIME_ZONE= "TIME WITH TIME ZONE";
    /**
     * 二进制
     */
    String BIT = "bit";
    String BINARY = "binary";
    String VARBINARY = "varbinary";
    String BLOB = "blob";
    String TEXT = "text";
    String CLOB = "clob";
}
