package com.tangzc.mpe.demo.autotable.dm;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yangdingming
 * @date 2024/2/27 15:46
 **/
@RestController
@RequestMapping("autotable/dm")
public class DMTableController {
    @Resource
    private DMTableRepository dmTableRepository;

    @GetMapping("add")
    public void add(String username) {
        DMTable entity = new DMTable();
        entity.setUsername(username);
        this.dmTableRepository.save(entity);
    }

    @GetMapping("list")
    public List<DMTable> list() {
        return this.dmTableRepository.list();
    }
}
