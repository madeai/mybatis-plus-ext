package com.tangzc.mpe.demo.autotable.dm;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tangzc.mpe.base.repository.BaseRepository;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author yangdingming
 * @date 2024/2/27 15:46
 **/
@DS("my-dm")
@Component
public class DMTableRepository extends BaseRepository<DMTableMapper, DMTable> {
}

@Mapper
interface DMTableMapper extends BaseMapper<DMTable> {

}
